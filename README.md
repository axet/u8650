# u8650

works:

- build
- recovery
- gsm (msm7227, voice / sms)
- wifi (BCM4329)
- bluetooth (BCM4329B1)
- audio (msm7227)
- gps (msm7227)
- sensors (accelerometer: mma8452, lux, proximity: APS-12D)
- compass (akm8975)
- camera preview (msm7227, 23060043SF-SAM-S)

not working:

- camera taking pictures and recording
- fm radio (BCM4329)

# Device Informaion

* Qualcomm Snapdragon S1 MSM7227
  * 600 MHz
  * GPU: Adreno 200 (133 MHz GPU, RAM: 0.25 MiB)
  * ARM11 600-800 MHz
  * 65 nm
  * ARMv6
  * ARM1136EJ-S
  * GPS (Gen 7 GPS module)

* GSM: CSD 9.6 kbps, Circuit Switched Data (CSD), GPRS, EDGE, EDGE Multi-slot Class 12, UMTS 384 kbps (W-CDMA). HSUPA (Cat. 6), HSDPA.

* 256MB RAM

* 512MB ROM

* 1400mAh Li-ion battery

* kernel: 2.6.35

* board id: MSM7X27_U8650.VerB

* FLASH ID: Hynix-0x5510bcad

* LCD ID: INNOLUX ILI9481

# Build

.repo/local_manifests/roomservice.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <remote  name="gitlab" fetch="https://gitlab.com/" />
  <project path="device/huawei/u8650" remote="gitlab" name="axet/android_device_huawei_u8650" revision="cm-9.1.0" />
  <project path="kernel/huawei/u8650" remote="gitlab" name="axet/android_kernel_huawei_u8650" revision="cm-9.1.0" />
  <project path="vendor/huawei/u8650" remote="gitlab" name="axet/proprietary_vendor_huawei" revision="cm-9.1.0" />
</manifest>
```
