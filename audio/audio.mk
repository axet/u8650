# Audio
PRODUCT_PACKAGES += \
    audio.a2dp.default \
    audio.primary.u8650 \
    audio_policy.u8650 \

PRODUCT_COPY_FILES += \
    device/huawei/u8650/audio/AudioFilter.csv:system/etc/AudioFilter.csv \
    device/huawei/u8650/audio/media_profiles.xml:system/etc/media_profiles.xml

# Audio
PRODUCT_COPY_FILES += \
    device/huawei/u8650/audio/AutoVolumeControl.txt:system/etc/AutoVolumeControl.txt

