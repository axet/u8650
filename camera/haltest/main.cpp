#include <gingerbread/CameraHardwareInterface.h>
#include <gingerbread/CameraService.h>
#include <hardware/camera.h>
#include <dlfcn.h>
#include <ui/Overlay.h>

const camera_module_t *HAL_MODULE_INFO_SYM;
android::sp<android::CameraHardwareInterface> qCamera;

void notify_cb(int32_t msgType, int32_t e1, int32_t e2, void* user) { // camera_notify_callback
  fprintf(stderr, "notify_cb %d %d %d %p\n", msgType, e1, e2, user);
  return;
}

void data_cb(int32_t, const camera_memory_t*, unsigned int, camera_frame_metadata_t*, void*) { // camera_data_callback
  fprintf(stderr, "data_cb\n");
  return;
}

void data_cb_timestamp(int64_t, int32_t, const camera_memory_t*, unsigned int, void*) { // camera_data_timestamp_callback
  fprintf(stderr, "data_cb_timestamp\n");
  return;
}

camera_memory_t * get_memory(int, size_t, unsigned int, void*) { // camera_request_memory
  fprintf(stderr, "get_memory\n");
  return 0;
}

int img = 0;

void data_cb2(int32_t t, const android::sp<android::IMemory>& p, void* u) { // camera_data_callback
  fprintf(stderr, "data_cb2 %d p:%p size:%d %p\n", t, p->pointer(), p->size(), u);
  if (t == android::CAMERA_MSG_COMPRESSED_IMAGE) {
    char buf[1024];
    sprintf(buf, "/data/local/tmp/image%d.jpg", img++);
    FILE *fp = fopen(buf, "w");
    fwrite(p->pointer(), p->size(), 1, fp);
    fclose(fp);
  }
  if (t == android::CAMERA_MSG_RAW_IMAGE) {
    char buf[1024];
    sprintf(buf, "/data/local/tmp/image%d.raw", img++);
    FILE *fp = fopen(buf, "w");
    fwrite(p->pointer(), p->size(), 1, fp);
    fclose(fp);
  }
  return;
}

void data_cb_timestamp2(nsecs_t timestamp, int32_t msgType, const android::sp<android::IMemory>& dataPtr, void* user) { // camera_data_timestamp_callback
  fprintf(stderr, "data_cb_timestamp2 %d %d p:%p size:%d %p\n", (int)(timestamp/1000), msgType, dataPtr->pointer(), dataPtr->size(), user);
  qCamera->releaseRecordingFrame(dataPtr);
  return;
}

int (dequeue_buffer)(struct preview_stream_ops* w,buffer_handle_t** buffer, int *stride) {
  fprintf(stderr, "dequeue_buffer\n");
  return 0;
}
int (enqueue_buffer)(struct preview_stream_ops* w,  buffer_handle_t* buffer) {
  fprintf(stderr, "enqueue_buffer\n");
  return 0;
}
int (cancel_buffer)(struct preview_stream_ops* w, buffer_handle_t* buffer) {
  fprintf(stderr, "cancel_buffer\n");
  return 0;
}
int (set_buffer_count)(struct preview_stream_ops* w, int count) {
  fprintf(stderr, "set_buffer_count\n");
  return 0;
}
int (set_buffers_geometry)(struct preview_stream_ops* pw, int w, int h, int format){
  fprintf(stderr, "set_buffers_geometry\n");
  return 0;
}
int (set_crop)(struct preview_stream_ops *w, int left, int top, int right, int bottom){
  fprintf(stderr, "set_crop\n");
  return 0;
}
int (set_usage)(struct preview_stream_ops* w, int usage){
  fprintf(stderr, "set_usage\n");
  return 0;
}
int (set_swap_interval)(struct preview_stream_ops *w, int interval){
  fprintf(stderr, "set_swap_interval\n");
  return 0;
}
int (get_min_undequeued_buffer_count)(const struct preview_stream_ops *w, int *count){
  fprintf(stderr, "get_min_undequeued_buffer_count\n");
  return 0;
}
int (lock_buffer)(struct preview_stream_ops* w, buffer_handle_t* buffer){
  fprintf(stderr, "lock_buffer\n");
  return 0;
}

void haltest(const char* camid) {
  fprintf(stderr, "compat: %p\n",::dlopen("libcamera_compat.so", RTLD_NOW|RTLD_GLOBAL));

  void *p = ::dlopen("/system/lib/hw/camera.msm7x27.so", RTLD_NOW);
  fprintf(stderr, "hal: %p\n", p);
  HAL_MODULE_INFO_SYM = (camera_module_t*)::dlsym(p, HAL_MODULE_INFO_SYM_AS_STR);
  fprintf(stderr, "hal id: %p\n", HAL_MODULE_INFO_SYM);
  fprintf(stderr, "number of camers: %d\n", HAL_MODULE_INFO_SYM->get_number_of_cameras());
  camera_info info;
  fprintf(stderr, "get_camera info(0): %d\n", HAL_MODULE_INFO_SYM->get_camera_info(0, &info));
  fprintf(stderr, "facing: %d\n", info.facing);
  fprintf(stderr, "orientation: %d\n", info.orientation);
  
  camera_device_t *mDevice = 0;
  fprintf(stderr, "open %s %d\n", camid, HAL_MODULE_INFO_SYM->common.methods->open(&HAL_MODULE_INFO_SYM->common, camid, (hw_device_t**)&mDevice ));
  fprintf(stderr, "open device = %p, tag=%x \n" , mDevice, mDevice->common.tag);
  
  fprintf(stderr, "get parameters: %s\n", mDevice->ops->get_parameters(mDevice));
  
  fprintf(stderr, "dump %d\n", mDevice->ops->dump(mDevice, 2));
  
  fprintf(stderr, "set callbacks\n");
  mDevice->ops->set_callbacks(mDevice, notify_cb, data_cb, data_cb_timestamp, get_memory, 0);
  
  preview_stream_ops_t ops;
  ops.dequeue_buffer = dequeue_buffer;
  ops.enqueue_buffer = enqueue_buffer;
  ops.cancel_buffer=cancel_buffer;
  ops.set_buffer_count=set_buffer_count;
  ops.set_buffers_geometry=set_buffers_geometry;
  ops.set_crop=set_crop;
  ops.set_usage=set_usage;
  ops.set_swap_interval=set_swap_interval;
  ops.get_min_undequeued_buffer_count=get_min_undequeued_buffer_count;
  ops.lock_buffer=lock_buffer;
  fprintf(stderr, "set_preview_window %d\n", mDevice->ops->set_preview_window(mDevice, &ops));
  
  fprintf(stderr, "start preview %d\n",mDevice->ops->start_preview(mDevice));

  int count=0;
  while(true){
    count++;
    if( count%5 == 0 ) {
      fprintf(stderr, "take picture %d\n",mDevice->ops->take_picture(mDevice));
    }
    usleep(1000000);
  }
}

android::sp<android::CameraHardwareInterface> (*LINK_openCameraHardware)(int id);

int (*LINK_getNumberOfCameras)();

class Surf : public android::BnSurface {
virtual android::sp<android::GraphicBuffer> requestBuffer(int, uint32_t, uint32_t, uint32_t, uint32_t) {
  fprintf(stderr, "reqbuf\n");
  return 0;
}
virtual android::status_t setBufferCount(int) {
  fprintf(stderr, "setbuf\n");
  return 0;
}
virtual android::status_t registerBuffers(const android::ISurface::BufferHeap&) {
  fprintf(stderr, "regbuf\n");
  return 0;
}
virtual void postBuffer(ssize_t) {
  fprintf(stderr, "post\n");
}
virtual void unregisterBuffers() {
  fprintf(stderr, "unreG\n");
}
virtual android::sp<android::OverlayRef> createOverlay(uint32_t, uint32_t, int32_t, int32_t) {
  fprintf(stderr, "createOverlay\n");
  return 0;
}
};

void camtest(int cameraId) {
   fprintf(stderr, "compat: %p\n",::dlopen("libcamera_compat.so", RTLD_NOW|RTLD_GLOBAL));
   
   void * libcameraHandle = ::dlopen("libcamera.so", RTLD_LAZY);
   fprintf(stderr, "loading libcamera at %p\n", libcameraHandle);
   if (!libcameraHandle) {
       fprintf(stderr, "FATAL ERROR: could not dlopen libcamera.so: %s\n", dlerror());
       return;
   }

   if (::dlsym(libcameraHandle, "openCameraHardware") != NULL) {
      *(void**)&LINK_openCameraHardware =
               ::dlsym(libcameraHandle, "openCameraHardware");
   } else if (::dlsym(libcameraHandle, "HAL_openCameraHardware") != NULL) {
      *(void**)&LINK_openCameraHardware =
               ::dlsym(libcameraHandle, "HAL_openCameraHardware");
   } else {
      fprintf(stderr, "FATAL ERROR: Could not find openCameraHardware\n");
      dlclose(libcameraHandle);
      return;
   }

   *(void**)&LINK_getNumberOfCameras = ::dlsym(libcameraHandle, "HAL_getNumberOfCameras");
   fprintf(stderr, "HAL_getNumberOfCameras: %d\n", LINK_getNumberOfCameras());

   fprintf(stderr, "open LINK_openCameraHardware %p\n", LINK_openCameraHardware);
   qCamera = LINK_openCameraHardware(cameraId);
   
   fprintf(stderr, "get camera parameters\n");
   android::CameraParameters cp = qCamera->getParameters();
   fprintf(stderr, "getPictureFormat: %s\n", cp.getPictureFormat());
   fprintf(stderr, "getPreviewFrameRate: %d\n", cp.getPreviewFrameRate());
   int w,h;
   cp.getPictureSize(&w, &h);
   fprintf(stderr, "getPictureSize: %d %d\n", w, h);
   android::Vector<android::Size> sizes;
   cp.getSupportedPictureSizes(sizes);
   for(int i=0;i<sizes.size();i++) {
     fprintf(stderr, "supported size: %d %d\n", sizes.itemAt(i).width, sizes.itemAt(i).height);
   }
   //cp.setPictureFormat("raw");
   fprintf(stderr, "setParameters: %d\n", qCamera->setParameters(cp));

   fprintf(stderr, "set callbacks\n");
   qCamera->setCallbacks(notify_cb, data_cb2, data_cb_timestamp2, (void*)0xabcdef);

   qCamera->enableMsgType(android::CAMERA_MSG_ERROR|android::CAMERA_MSG_ZOOM|android::CAMERA_MSG_FOCUS|android::CAMERA_MSG_POSTVIEW_FRAME|android::CAMERA_MSG_PREVIEW_FRAME|android::CAMERA_MSG_RAW_IMAGE|android::CAMERA_MSG_COMPRESSED_IMAGE);

   android::sp<android::Overlay> dummy;
   qCamera->setOverlay(0);

   android::sp<android::IMemoryHeap> heap = qCamera->getRawHeap();
   fprintf(stderr, "heap %p\n", heap == 0 ? 0 : heap->getBase());

   android::sp<android::IMemoryHeap> prev = qCamera->getPreviewHeap();
   fprintf(stderr, "prev %p\n", prev == 0 ? 0 : prev->getBase());

   fprintf(stderr, "dump:\n");
   android::Vector<android::String16> args;
   qCamera->dump(2, args);
   fprintf(stderr, "dump done\n");

   fprintf(stderr, "start preview\n");
   qCamera->startPreview();

   fprintf(stderr, "init done\n");

   Surf sur;

   int count=0;
   while(true){
     count++;
     if(count%10==3) {
       fprintf(stderr, "auto focus\n");
       qCamera->autoFocus();
     }
     if(count%10==6) {
       fprintf(stderr, "take picture\n");
       qCamera->takePicture(&sur);
     }
     if(count%10==9) {
       qCamera->enableMsgType(android::CAMERA_MSG_PREVIEW_FRAME);
       qCamera->startPreview();
     }
     usleep(1000000);
   }
}

int main(int argc, char **argv) {
  if(argc < 2)
    return 1;
  if(strcmp(argv[1], "hal") == 0)
    haltest("0");
  if(strcmp(argv[1], "cam") == 0)
    camtest(0);
  return 0;
}

