LOCAL_PATH := $(call my-dir)
include $(call all-subdir-makefiles)

include $(CLEAR_VARS)

LOCAL_C_FLAGS        += -O3
LOCAL_MODULE_TAGS    := optional
LOCAL_MODULE         := haltest
LOCAL_SRC_FILES      := main.cpp

LOCAL_SHARED_LIBRARIES := liblog libdl libutils libcamera_client libbinder libcutils libhardware libui libgui
LOCAL_C_INCLUDES       := frameworks/base/services/ frameworks/base/include
LOCAL_C_INCLUDES       += hardware/libhardware/include/ hardware
LOCAL_C_INCLUDES       += frameworks/base/services/camera/libcameraservice/
LOCAL_C_INCLUDES       += device/huawei/u8650/include/

include $(BUILD_EXECUTABLE)
