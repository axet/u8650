LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
        main.cpp \

LOCAL_CFLAGS := -DLOG_TAG=\"libQcomUI\"

LOCAL_LDFLAGS        += -zglobal

LOCAL_SHARED_LIBRARIES := \
        liblog libdl libutils libcamera_client libbinder libcutils libhardware liboverlay libui

LOCAL_C_INCLUDES := $(TOP)/hardware/qcom/display/libgralloc \
                    $(TOP)/frameworks/base/services/surfaceflinger \
                    $(TOP)/external/skia/include/core \
                    $(TOP)/external/skia/include/images

LOCAL_MODULE := libcamera_compat
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
