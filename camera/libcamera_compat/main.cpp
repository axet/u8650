#include <hardware/hardware.h>
#include <hardware/camera.h>
#include <binder/IMemory.h>
#include <surfaceflinger/ISurface.h>

#include <ui/Rect.h>
#include <ui/GraphicBufferMapper.h>

#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>

extern "C" {
    #include <linux/android_pmem.h>
}

#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>

extern "C" {
bool _ZN7overlay7Overlay11queueBufferEPK13native_handle(void *p); // overlay::Overlay::queueBuffer(native_handle const*)
bool _ZN7android7Overlay11queueBufferEPv(void* p) {
  fprintf(stderr, "Overlay::queueBuffer\n");
  return _ZN7overlay7Overlay11queueBufferEPK13native_handle(p);
}

bool _ZN7overlay7Overlay5setFdEii(int fd, int channel); // overlay::Overlay::setFd(int, int)
bool _ZN7android7Overlay5setFdEi(int fd) { // android::Overlay::setFd(int)
  return _ZN7overlay7Overlay5setFdEii(fd, 0);
}

bool _ZN7overlay7Overlay7setCropEjjjj(unsigned int, unsigned int, unsigned int, unsigned int); // overlay::Overlay::setCrop(unsigned int, unsigned int, unsigned int, unsigned int)
bool _ZN7android7Overlay7setCropEjjjj(unsigned int i1 , unsigned int i2, unsigned int i3, unsigned int i4) {
  fprintf(stderr, "Overlay::setCrop\n");
  return _ZN7overlay7Overlay7setCropEjjjj(i1, i2, i3, i4);
}

void _ZN7android8ISurfacX10BufferHeapC1EjjiiijjRKNS_2spINS_11IMemoryHeapEEE(unsigned int i1, unsigned int i2 , int i3, int i4, int i5, unsigned int i6, unsigned int i7, void*p1);
void _ZN7android8ISurface10BufferHeapC1EjjiiijjRKNS_2spINS_11IMemoryHeapEEE(unsigned int i1, unsigned int i2 , int i3, int i4, int i5, unsigned int i6, unsigned int i7, void*p1) {
  fprintf(stderr, "BufferHeap::BufferHeap\n");
  _ZN7android8ISurfacX10BufferHeapC1EjjiiijjRKNS_2spINS_11IMemoryHeapEEE(i1, i2, i3, i4,i5, i6,i7,p1);
}

void _ZN7android8ISurfacX10BufferHeapD1Ev();
void _ZN7android8ISurface10BufferHeapD1Ev() {
  fprintf(stderr, "BufferHeap::~BufferHeap\n");
 _ZN7android8ISurfacX10BufferHeapD1Ev();
}

}

namespace android {
namespace ISurfacX {

    class BufferHeap {
    public:
        enum {
            /* rotate source image */
            ROT_0     = 0,
            ROT_90    = HAL_TRANSFORM_ROT_90,
            ROT_180   = HAL_TRANSFORM_ROT_180,
            ROT_270   = HAL_TRANSFORM_ROT_270,
        };
        BufferHeap();
        
        BufferHeap(uint32_t w, uint32_t h,
                int32_t hor_stride, int32_t ver_stride, 
                PixelFormat format, const sp<IMemoryHeap>& heap);
        
        BufferHeap(uint32_t w, uint32_t h,
                int32_t hor_stride, int32_t ver_stride, 
                PixelFormat format, uint32_t transform, uint32_t flags,
                const sp<IMemoryHeap>& heap);
        
        ~BufferHeap(); 
        
        uint32_t w;
        uint32_t h;
        int32_t hor_stride;
        int32_t ver_stride;
        PixelFormat format;
        uint32_t transform;
        uint32_t flags;
        sp<IMemoryHeap> heap;
    };

    BufferHeap::BufferHeap() 
        : w(0), h(0), hor_stride(0), ver_stride(0), format(0),
        transform(0), flags(0) 
    {     
    }

    BufferHeap::BufferHeap(uint32_t w, uint32_t h,
        int32_t hor_stride, int32_t ver_stride,
        PixelFormat format, const sp<IMemoryHeap>& heap)
        : w(w), h(h), hor_stride(hor_stride), ver_stride(ver_stride),
          format(format), transform(0), flags(0), heap(heap) 
    {
    }

    BufferHeap::BufferHeap(uint32_t w, uint32_t h,
        int32_t hor_stride, int32_t ver_stride,
        PixelFormat format, uint32_t transform, uint32_t flags,
        const sp<IMemoryHeap>& heap)
        : w(w), h(h), hor_stride(hor_stride), ver_stride(ver_stride),
          format(format), transform(transform), flags(flags), heap(heap) 
    {
    }

    BufferHeap::~BufferHeap() 
    {     
    }

}
}

