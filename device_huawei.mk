# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

DEVICE_PACKAGE_OVERLAYS := device/huawei/u8650/overlay

PRODUCT_MANUFACTURER := huawei

$(call inherit-product, device/huawei/u8650/build_prop.mk)

# HW decoding
PRODUCT_PACKAGES += \
    libmm-omxcore \
    libstagefrighthw \
    libopencorehw \
    libOmxCore \
    libOmxVidEnc \
    libgenlock \
    liboverlay \
    libtilerenderer
# Misc
PRODUCT_PACKAGES += \
    dexpreopt

PRODUCT_LOCALES := en_US

PRODUCT_LOCALES += mdpi

# We have enough storage space to hold precise GC data
PRODUCT_TAGS += dalvik.gc.type-precise
DISABLE_DEXPREOPT := false

# Sysctl
PRODUCT_COPY_FILES += \
    device/huawei/u8650/etc/init.d/01sysctl:system/etc/init.d/01sysctl \
    device/huawei/u8650/etc/sysctl.conf:system/etc/sysctl.conf

# Install the features available on this device.
PRODUCT_COPY_FILES += \
    frameworks/base/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/base/data/etc/android.hardware.telephony.cdma.xml:system/etc/permissions/android.hardware.telephony.cdma.xml \
    frameworks/base/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/base/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/base/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/base/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/base/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:/system/etc/permissions/android.software.live_wallpaper.xml

# Vold 
PRODUCT_COPY_FILES += \
    device/huawei/u8650/etc/vold.fstab:system/etc/vold.fstab

$(call inherit-product-if-exists, device/huawei/u8650/recovery/recovery.mk)
$(call inherit-product-if-exists, device/huawei/u8650/audio/audio.mk)
$(call inherit-product-if-exists, vendor/huawei/u8650/u8650.mk)
