# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Proprietary and common side of the device
$(call inherit-product, device/huawei/u8650/device_huawei.mk)

# Discard inherited values and use our own instead.
PRODUCT_NAME := cm_u8650
PRODUCT_DEVICE := u8650
PRODUCT_MODEL := u8650

# Libs
PRODUCT_PACKAGES += \
    lights.default \
    lights.msm7x27 \
    lights.u8650 \
    hwcomposer.msm7x27 \
    copybit.msm7k \
    copybit.msm7x27 \
    gralloc.msm7k \
    gralloc.msm7x27 \
    gps.u8650 \
    sensors.default

PRODUCT_PACKAGES += remove_unused_apps

# Install the features available on this device.
PRODUCT_COPY_FILES += \
    frameworks/base/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml

# Init
PRODUCT_COPY_FILES += \
    device/huawei/u8650/root/init.rc:root/init.rc \
    device/huawei/u8650/root/init.huawei.rc:root/init.huawei.rc \
    device/huawei/u8650/root/init.huawei.usb.rc:root/init.huawei.usb.rc \
    device/huawei/u8650/root/ueventd.huawei.rc:root/ueventd.huawei.rc

# Keypad files
PRODUCT_COPY_FILES += \
    device/huawei/u8650/keypad/keylayout/7k_handset.kl:system/usr/keylayout/7k_handset.kl \
    device/huawei/u8650/keypad/keylayout/Generic.kl:system/usr/keylayout/Generic.kl \
    device/huawei/u8650/keypad/keylayout/synaptics.kl:system/usr/keylayout/synaptics.kl \
    device/huawei/u8650/keypad/idc/kp_test_input.idc:system/usr/idc/kp_test_input.idc \
    device/huawei/u8650/keypad/idc/msm_touchscreen.idc:system/usr/idc/msm_touchscreen.idc \
    device/huawei/u8650/keypad/idc/qwerty.idc:system/usr/idc/qwerty.idc \
    device/huawei/u8650/keypad/idc/qwerty2.idc:system/usr/idc/qwerty2.idc \
    device/huawei/u8650/keypad/idc/sensors.idc:system/usr/idc/sensors.idc \
    device/huawei/u8650/keypad/idc/surf_keypad.idc:system/usr/idc/surf_keypad.idc \
    device/huawei/u8650/keypad/idc/synaptics.idc:system/usr/idc/synaptics.idc \
    device/huawei/u8650/keypad/idc/synaptics-rmi4-ts.idc:system/usr/idc/synaptics-rmi4-ts.idc \
    device/huawei/u8650/keypad/idc/synaptics-rmi-touchscreen.idc:system/usr/idc/synaptics-rmi-touchscreen.idc \
    device/huawei/u8650/keypad/idc/touchscreen-keypad.idc:system/usr/idc/touchscreen-keypad.idc \
    device/huawei/u8650/keypad/idc/ts_test_input.idc:system/usr/idc/ts_test_input.idc \
